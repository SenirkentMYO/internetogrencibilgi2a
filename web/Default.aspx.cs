﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace web
{

    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Application["sayac"] == null)
                Application["sayac"] = 1;

            else
            {
                int sayi = (int)Application["sayac"];
                sayi++;
                Application["sayac"] = sayi;
                lblSayac.Text = Application["sayac"].ToString();
            }

        }
        protected void Timer1_Tick(object sender, EventArgs e)
        {
            List<Vemailsifre> p = new List<Vemailsifre>();
            using (SqlConnection baglanti = new SqlConnection(ConfigurationManager.ConnectionStrings["baglanticumlesi"].ToString()))
            {
                baglanti.Open();

                string sorguCumlesi = "Select*from emailsifre where ogr_email= '" + txtgmail.Text + "' and ogr_sifre= '" + txtsifre.Text + "'";
                SqlCommand komut = new SqlCommand(sorguCumlesi, baglanti);
                SqlDataReader satir = komut.ExecuteReader();

                while (satir.Read())
                {
                    Vemailsifre s = new Vemailsifre();
                    s.ogr_tc = satir["ogr_tc"].ToString();
                    s.ogr_email = satir["ogr_email"].ToString();
                    s.ogr_sifre = satir["ogr_sifre"].ToString();
                    s.ogr_ad = satir["ogr_ad"].ToString();
                    s.ogr_soyad = satir["ogr_soyad"].ToString();
                    p.Add(s);

                }
                baglanti.Close();
                baglanti.Dispose();

                Repeater1.DataSource = p;
                Repeater1.DataBind();
            }

        }

        protected void btngiris_Click(object sender, EventArgs e)
        {
            List<emailsifre> emails = new List<emailsifre>();
            using (SqlConnection baglanti = new SqlConnection(ConfigurationManager.ConnectionStrings["baglanticumlesi"].ToString()))
            {
                baglanti.Open();
                string sorguCumlem = "Select * from emailsifre ";

                SqlCommand komut = new SqlCommand(sorguCumlem, baglanti);
                SqlDataReader satir = komut.ExecuteReader();
                               while (satir.Read())
                {
                    emailsifre k = new emailsifre();
                    k.ogr_tc = satir["ogr_tc"].ToString();
                    k.ogr_email = satir["ogr_email"].ToString();
                    k.ogr_sifre = satir["ogr_sifre"].ToString();
                    k.ogr_ad = satir["ogr_ad"].ToString();
                    k.ogr_soyad = satir["ogr_soyad"].ToString();
                    emails.Add(k);

                }
                baglanti.Close();
                baglanti.Dispose();


                if (emails.Count > 0)
                {
                    Session["GirilenUye"] = emails[0];
                    Response.Redirect("Uye.aspx");

                    lblhosgeldin.Text = "HOŞGELDİNİZ.... " + emails[0].ogr_ad + "  " + emails[0].ogr_soyad;

                }
                else
                {
                    lblhosgeldin.Text = "YANLIŞ GİRİŞ....";


                }

            }

        }


        protected void Repeater1_ItemCommand(object source, RepeaterCommandEventArgs e)
        {

        }
    }
}